#!/bin/bash

# Find which OS is used
# This gives us two variables $ID which contains a string naming the OS
# and $VERSION_ID which contains a string with the OS version
# e.g. for Ubuntu 18.04:
#   $ID=ubuntu
#   $VERSION_ID=18.04
source /etc/os-release


########################################################################
# Parse the arguments
########################################################################
case $1 in
-h|--help)
    echo "KiCad dependency installer"
    echo ""
    echo "This script will install the dependencies needed to build"
    echo "KiCad from source. The dependencies installed can be configured"
    echo "by using the following flags to enable/disable installation:"
    echo "--use-wxpython:  Install the wxPython library"
    echo "--use-wxphoenix: Install wxPhoenix library"
    echo "--use-python2:   Install the Python 2 interpreter"
    echo "--use-python3:   Install the Python 3 interpreter"
    echo "--use-ngspice:   Install the ngspice library"
    echo "--use-oce:       Install OpenCascade Community Edition"
    echo "--use-occ:       Install OpenCascade"
    echo "--install-xvfb:  Install xvfb for running the unit tests headless"
    echo ""
    echo "For example, to install OpenCascade and not OCE:"
    echo "    $0 --use-occ=1 --use-oce=0"
    echo ""
    echo "Calling with no flags is the equivalent of providing:"
    echo "  --use-wxphoenix=1 --use-python3=1 --use-ngspice=1 --use-occ=1"
    exit
esac

USE_PYTHON2=0
USE_PYTHON3=1
USE_WXPYTHON=0
USE_WXPHOENIX=1
USE_NGSPICE=1
USE_OCC=1
USE_OCE=0
INSTALL_XVFB=0


while (( "$#" )); do
    case "$1" in
    --use-wxpython=*)
        USE_WXPYTHON="${1#*=}"
        if [[ $USE_PYTHON == 0 ]];
        then
            USE_PYTHON="${1#*=}"
        fi
        shift
        ;;
    --use-wxphoenix=*)
        USE_WXPHOENIX="${1#*=}"
        if [[ $USE_PYTHON == 0 ]];
        then
            USE_PYTHON="${1#*=}"
        fi
        shift
        ;;
    --use-python2=*)
        USE_PYTHON2="${1#*=}"
        shift
        ;;
    --use-python3=*)
        USE_PYTHON3="${1#*=}"
        shift
        ;;
    --use-ngspice=*)
        USE_NGSPICE="${1#*=}"
        shift
        ;;
    --use-oce=*)
        USE_OCE="${1#*=}"
        shift
        ;;
    --use-occ=*)
        USE_OCC="${1#*=}"
        shift
        ;;
    --install-xvfb=*)
        INSTALL_XVFB="${1#*=}"
        shift
        ;;
    --*) # unsupported flags
      echo "Error: Unknown flag $1" >&2
      exit 1
      break
      ;;
  esac
done


########################################################################
# Verify the options
########################################################################

# Print out the boolean logic
to_bool[0]="no"
to_bool[1]="yes"

echo "Configured options:"
echo "    Install Python 2: ${to_bool[$USE_PYTHON2]}"
echo "    Install Python 3: ${to_bool[$USE_PYTHON3]}"
echo "    Install wxPython: ${to_bool[$USE_WXPYTHON]}"
echo "    Install wxPhoenix: ${to_bool[$USE_WXPHOENIX]}"
echo "    Install ngspice: ${to_bool[$USE_NGSPICE]}"
echo "    Install OCE: ${to_bool[$USE_OCE]}"
echo "    Install OCC (OpenCascade): ${to_bool[$USE_OCC]}"
echo "    Install xvfb (X Virtual Framebuffer): ${to_bool[$INSTALL_XVFB]}"

echo ""


########################################################################
# Test if 'sudo' is available, work around otherwise
#
# Ubuntu docker images run as root and do not have the sudo command available
########################################################################
if ! command -v sudo >/dev/null 2>&1;
then
    # sudo is not available
    if [[ "$EUID" == "0" ]];
    then
        # We are running as root, so use a dummy function that makes 'sudo command' just call 'command'
        function sudo()
        {
            $@
        }
    else
        echo "Sudo is not available, please run this script as root." >&2
        exit 1
    fi
fi


########################################################################
# Install Fedora dependencies
########################################################################
if [[ $ID == "fedora" ]];
then
    echo "Detected Fedora operating system"
    echo ""

    # Install development tools
    PACKAGES="@development-tools \
        findutils \
        coreutils \
        grep \
        git \
        cmake \
        lemon \
        make \
        ninja-build \
        ccache \
        automake \
        gcc-c++ \
        gdb \
        clang \
        clang-libs \
        clang-tools-extra \
        git-clang-format \
        libasan \
        doxygen \
        graphviz"

    # These are the core dependencies
    PACKAGES="$PACKAGES \
        mesa-libGLw-devel \
        glew-devel \
        glm-devel \
        libcurl-devel \
        cairo-devel \
        boost-devel \
        openssl-devel \
        wxGTK3-devel \
        gtk3-devel"

    # Install the x virtual framebuffer if requested
    if [ $INSTALL_XVFB == 1 ];
    then
        PACKAGES="$PACKAGES \
            xorg-x11-server-Xvfb"
    fi

    # Install Python 2 and the appropriate wxPython packages
    if [ $USE_PYTHON2 == 1 ];
    then
        # If using Python, we need SWIG to compile the interface
        PACKAGES="$PACKAGES \
            swig \
            python2-devel"

        # Plain old wxPython
        if [ $USE_WXPYTHON == 1 ];
        then
            PACKAGES="$PACKAGES wxPython-devel"
        fi

        # The new wxPhoenix
        if [ $USE_WXPHOENIX == 1 ];
        then
            PACKAGES="$PACKAGES python2-wxpython4"
        fi
    fi

    # Install Python 3 and the appropriate wxPython packages
    if [ $USE_PYTHON3 == 1 ];
    then
        # If using Python, we need SWIG to compile the interface
        PACKAGES="$PACKAGES \
            swig \
            python3-devel"

        # Plain old wxPython doesn't exist in Fedora for Python 3
        if [ $USE_WXPYTHON == 1 ];
        then
            echo "Error: wxPython is not available for Python 3" >&2
            exit 1
        fi

        # The new wxPhoenix
        if [ $USE_WXPHOENIX == 1 ];
        then
            PACKAGES="$PACKAGES python3-wxpython4"
        fi
    fi

    # Fedora's packaged ngspice shared library
    if [ $USE_NGSPICE == 1 ];
    then
        PACKAGES="$PACKAGES \
            libngspice \
            libngspice-devel"
    fi

    # OpenCascade
    if [ $USE_OCC == 1 ];
    then
        if [ $USE_OCE == 1 ];
        then
            echo "Error: Fedora cannot have both OCE and OCC installed" >&2
            exit 1
        fi

        PACKAGES="$PACKAGES opencascade-devel"

    fi

    # OCE has been retired on Fedora in favor of opencascade
    if [ $USE_OCE == 1 ];
    then
        echo "OCE is retired on Fedora"
        PACKAGES="$PACKAGES OCE-devel"
    fi

    echo "Installing packages: " $PACKAGES
    echo ""

    sudo dnf -yq install $PACKAGES


########################################################################
# Install Ubuntu dependencies
########################################################################
elif [[ $ID == "ubuntu" ]];
then
    echo "Detected Ubuntu $VERSION_ID operating system"
    echo ""

    # Install development tools
    PACKAGES="build-essential \
        git \
        cmake \
        ccache \
        ninja-build \
        doxygen \
        graphviz \
        make \
        lemon \
        automake \
        gdb \
        clang \
        clang-tools \
        clang-format"

    # These are the core dependencies
    PACKAGES="$PACKAGES \
        mesa-common-dev \
        libcairo2-dev \
        libglu1-mesa-dev \
        libgl1-mesa-dev \
        libglew-dev \
        libglm-dev \
        libx11-dev \
        libssl-dev \
        libcurl4-openssl-dev \
        libboost-all-dev \
        libwxbase3.0-dev \
        libwxgtk3.0-gtk3-dev \
        libbz2-dev \
        libgtk-3-dev"

    # Install the x virtual framebuffer if requested
    if [ $INSTALL_XVFB == 1 ];
    then
        PACKAGES="$PACKAGES \
            xvfb \
            xauth"
    fi

    # Install Python 2 and the appropriate wxPython packages
    if [ $USE_PYTHON2 == 1 ];
    then
        # If using Python, we need SWIG to compile the interface
        PACKAGES="$PACKAGES \
            python-dev \
            swig"

        # Plain old wxPython for Python 2
        if [ $USE_WXPYTHON == 1 ];
        then
            PACKAGES="$PACKAGES python-wxgtk3.0-dev"
        fi

        # The new wxPhoenix for Python 2
        if [ $USE_WXPHOENIX == 1 ];
        then
            PACKAGES="$PACKAGES python-wxgtk4.0"
        fi
    fi

    # Install Python 3 and the appropriate wxPython packages
    if [ $USE_PYTHON3 == 1 ];
    then
        # If using Python, we need SWIG to compile the interface
        PACKAGES="$PACKAGES \
            python3-dev \
            swig"

        # Plain old wxPython doesn't exist in Ubuntu for Python 3
        if [ $USE_WXPYTHON == 1 ];
        then
            echo "Error: wxPython is not available for Python 3" >&2
            exit 1
        fi

        # The new wxPhoenix
        if [ $USE_WXPHOENIX == 1 ];
        then
            PACKAGES="$PACKAGES python3-wxgtk4.0"
        fi
    fi

    # Install an ngspice library
    if [ $USE_NGSPICE == 1 ];
    then
        if [[ $VERSION_ID == "18.04" ]];
        then
            # Add the PPA that provides the ngspice library on 18.04
            echo "Adding PPA for ngspice"
            sudo add-apt-repository -y ppa:js-reynaud/ngspice
            PACKAGES="$PACKAGES \
                libngspice-kicad"
        else
            PACKAGES="$PACKAGES \
                libngspice0-dev"
        fi
    fi

    # OpenCascade
    if [ $USE_OCC == 1 ];
    then
        # OCC must be gotten through a PPA for 18.04
        if [[ $VERSION_ID == "18.04" ]];
        then
            # Add the OCCT PPA that provides OCC on 18.04
            echo "Adding FreeCad PPA for OCC"
            sudo add-apt-repository -y ppa:freecad-maintainers/occt-releases
        elif [[ $VERSION_ID == "18.10" ]];
        then
            # There is no OCCT for 18.10 (and 18.10 isn't even supported anymore)
            echo "Error: OCC is not available for Ubuntu 18.10" >&2
            exit 1
        fi

        if [ $USE_OCE == 1 ];
        then
            echo "Error: Ubuntu cannot have both OCE and OCC installed" >&2
            exit 1
        fi

        PACKAGES="$PACKAGES \
            libocct-data-exchange-dev \
            libocct-draw-dev \
            libocct-foundation-dev \
            libocct-modeling-algorithms-dev \
            libocct-modeling-data-dev \
            libocct-ocaf-dev \
            libocct-visualization-dev"
    fi

    # If the user really wants OCE, then give it to them
    if [ $USE_OCE == 1 ];
    then
        PACKAGES="$PACKAGES \
            liboce-foundation-dev \
            liboce-modeling-dev \
            liboce-ocaf-lite-dev \
            liboce-ocaf-dev \
            liboce-visualization-dev"
    fi

    # Actually install the packages
    echo "Installing packages: " $PACKAGES
    echo ""

    sudo apt-get -y -qq update
    sudo apt-get -y -qq install $PACKAGES

########################################################################
# End of the if statement
########################################################################
fi
