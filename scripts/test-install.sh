#!/bin/bash

# This script will test that all dependencies have been installed onto
# a system by running a fully-specified CMake configure command

source /etc/os-release

CMAKE_ARGS="-DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
    -DKICAD_SANITIZE=ON \
    -DKICAD_STDLIB_LIGHT_DEBUG=ON \
    -DKICAD_SCRIPTING=ON \
    -DKICAD_SCRIPTING_MODULES=ON \
    -DKICAD_SCRIPTING_PYTHON3=ON \
    -DKICAD_SCRIPTING_WXPYTHON=ON \
    -DKICAD_SCRIPTING_WXPYTHON_PHOENIX=ON \
    -DKICAD_SCRIPTING_ACTION_MENU=ON \
    -DBUILD_GITHUB_PLUGIN=ON \
    -DKICAD_USE_OCE=OFF \
    -DKICAD_USE_OCC=ON \
    -DKICAD_SPICE=ON"

# On Ubuntu 18.04 we use OCC from the PPA, so we have
# to manually specify the include directory to search
if [[ $ID == "ubuntu" && $VERSION_ID == "18.04" ]];
then
    CMAKE_ARGS="$CMAKE_ARGS \
        -DOCC_INCLUDE_DIR=/usr/include/occt"
fi

git clone https://gitlab.com/kicad/code/kicad.git kicad
cd kicad

mkdir -p build/test
cd build/test

cmake $CMAKE_ARGS ../../
